#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "test.h"

using namespace CppUnit;

CPPUNIT_TEST_SUITE_REGISTRATION(LongTest);

void LongTest::setUp() {
	a = new longe(0);
	b = new longe(0);
	c = new longe(0);
}

void LongTest::tearDown() {
	delete a;
	delete b;
	delete c;
}

void LongTest::Addition() {
	*a = longe ("999999999999999");
	*b = longe ("1");
	*c = longe ("1000000000000000");
	CPPUNIT_ASSERT(*a+*b==*c);
	*a = longe ("123456789");
	*b = longe ("-123456789");
	*c = longe("0");
	CPPUNIT_ASSERT(*a+*b==*c);
	*a = longe("-1234894");
	*b = longe("-1234894");
	*c = longe("-2469788");
	CPPUNIT_ASSERT(*a+*b==*c);
}

void LongTest::Subtraction() {
	*a = longe("1000000000000000");
	*b = longe("999999999999999");
	*c = longe("1");
	CPPUNIT_ASSERT(*a-*b==*c);
	*a = longe("1234894");
	*b = longe("-1234894");
	*c = longe("2469788");
	CPPUNIT_ASSERT(*a-*b==*c);
	*a = longe("-1234894");
	*b = longe("-1234894");
	*c = longe("0");
	CPPUNIT_ASSERT(*a-*b==*c);
}

void LongTest::Multiplication() {
	*a = longe("1234894");
	*b = longe("123489");
	*c = longe("152495825166");
	CPPUNIT_ASSERT((*a)*(*b)==*c);
	*a = longe("123489421334455678678564564");
	*b = longe("0");
	*c = longe("0");
	CPPUNIT_ASSERT((*a)*(*b)==*c);
}

void LongTest::Division() {
	*a = longe ("98765430000348700006543");
	*b = longe ("123456789");
	*c = longe ("799999990282824");
	CPPUNIT_ASSERT((*a)/(*b) == *c);
	*a = longe ("123456789");
	*b = longe ("9876543000034");
	*c = longe ("0");
	CPPUNIT_ASSERT((*a)/(*b) == *c);
	*a = longe ("987654321");
	*b = longe ("1");
	*c = longe ("987654321");
	CPPUNIT_ASSERT((*a)/(*b) == *c);
}

void LongTest::More() {
	*a = longe ("9876543219876543211");
	*b = longe ("9876543219876543210");
	CPPUNIT_ASSERT(*a>*b);

}
void LongTest::Less() {
	*a = longe ("100000000000000000000");
	*b = longe ("100000000000000000001");
	CPPUNIT_ASSERT(*a<*b);
}

void LongTest::MoreEqual() {
	*a = longe ("123456789123456789");
	*b = longe ("123456789123456788");
	CPPUNIT_ASSERT(*a>=*b);
	*a = longe ("123456789123456789");
	*b = longe ("123456789123456789");
	CPPUNIT_ASSERT(*a>=*b);
}

void LongTest::LessEqual() {
	*a = longe ("123456789123456788");
	*b = longe ("123456789123456789");
	CPPUNIT_ASSERT(*a<=*b);
	*a = longe ("123456789123456789");
	*b = longe ("123456789123456789");
	CPPUNIT_ASSERT(*a<=*b);
}

void LongTest::Equal() {
	*a = longe ("123456789123456789");
	*b = longe ("123456789123456789");
	CPPUNIT_ASSERT(*a==*b);
}

void LongTest::NotEqual() {
	*a = longe ("223456789123456789");
	*b = longe ("123456789123456789");
	CPPUNIT_ASSERT(*a!=*b);
}    

int main( int argc, char **argv) {

    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry&  registry=CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner.run();

   return 0;
}
