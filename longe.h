﻿#include <cstdlib>
#include <string.h>

#include <utility>
#include <iostream>
#include <string>

#define MOD 10000

using namespace std;
/**
* <p> Описание класса longe
* @author Ким Игорь Андреевич
* @version 1.0.0
*/
class longe
{
	int *mas;
	int sign;
	int size;
public:
    /**
    * Конструктор преобразования и конструктор по умолчанию
    */
	longe( const int =  0);
	/**
    * Конструктор преобразования int в longe
    */
	longe( const char* );
	/**
    * Деструктор. Высвобождает выделенную память
    */
	~longe();
	/**
	* Перегрузка операции сложения для объектов класса longe.
	* @param b второе слагаемое.
	* @return сумму двух объектов класса longe.
	* @see longe::operator-	(const longe &b)		
	*/
	longe operator + ( const longe& );
	/**
	* Перегрузка операции сложения для объектов класса longe.
	* @param b вычитаемое.
	* @return разницу двух объектов класса longe.
	* @see longe::operator+	(const longe &b)
	*/
	longe operator - ( const longe& );
	/**
	* Перегрузка операции умножения для объектов класса longe.
	* @param b множитель.
	* @return произведение двух объектов класса longe.
	* @see longe::operator/	(const longe &b)
	*/
	longe operator * ( const longe& );
	/**
	* Перегрузка операции деления для объектов класса longe.
	* @param b делитель.
	* @return частное двух объектов класса longe.
	* @see longe::operator*	(const longe &b)
	*/
	longe operator / ( const longe& );
	/**
	* Перегрузка операции деления для объектов класса longe.
	* @param b делитель.
	* @return остаток от деления двух объектов класса longe.
	* @see longe::operator/	(const longe &b)
	*/
	longe operator % ( const longe& );
	//pair< longe, longe > operator divmod ( const longe& );
	/**
	* Перегрузка операции отрицания.
	* Изменяет знак объекта класса longe на противоположный.
	* @return объект с измененным знаком.
	*/
	longe operator - ();
	/**
	* Перегрузка операции сравнения на "больше"
	* @return true если "больше", и false, иначе.
	* @see longe::operator<	(const longe &b)
	* @see longe::operator<= (const longe &b)
	* @see longe::operator>= (const longe &b)	
	*/
	bool operator > ( const longe& );
	/**
	* Перегрузка операции сравнения на "меньше"
	* @return true если "меньше", и false, иначе.
	* @see longe::operator>	(const longe &b)
	* @see longe::operator<= (const longe &b)
	* @see longe::operator>= (const longe &b)
	*/
	bool operator < ( const longe& );
	/**
	* Перегрузка операции сравнения на "больше-равно"
	* @return true если "больше-равно", и false, иначе.
	* @see longe::operator<	(const longe &b)
	* @see longe::operator<= (const longe &b)
	* @see longe::operator> (const longe &b)
	*/
	bool operator >= ( const longe& );
	/**
	* Перегрузка операции сравнения на "меньше-равно"
	* @return true если "меньше-равно", и false, иначе.
	* @see longe::operator<	(const longe &b)
	* @see longe::operator< (const longe &b)
	* @see longe::operator>= (const longe &b)
	*/
	bool operator <= ( const longe& );
	/**
	* Перегрузка операции проверки на равенство
	* @return true если "равно", и false, иначе.
	* @see bool longe::operator!= (const longe &b)
	*/
	bool operator == ( const longe& );
	/**
	* Перегрузка операции сравнения 
	* @return true если "не равно", и false, иначе.
	* @see longe::operator== (const longe &b)
	*/
	bool operator != ( const longe& );
	/**
        * Перегрузка операции присваивания для объектов класса longe
	* @return this с новым значением.
	*/
	longe& operator = ( const longe& );
	/**
	* Перегрузка оператора >> для объектов класса longe.
	* Перенаправление потока.	
	*/
	friend istream& operator >> ( istream&, longe& );
	/**
	* Перегрузка оператора << для объектов класса longe.
	* Перенаправление потока.	
	*/
	friend ostream& operator << ( ostream&, const longe& );
	/**
	* Перегрузка операции += для объектов класса longe.
	* @param b слагаемое.
	* @return this,увеличенное на b.
	* @see longe::operator+	(const longe &b)
	*/
	longe& operator += ( const longe& );
	/**
	* Перегрузка операции -= для объектов класса longe.
	* @param b вычитаемое.
	* @return this,уменьшенное на b.
	* @see longe::operator- (const longe &b)
	*/
	longe& operator -= ( const longe& );
	/**
	* Перегрузка операции *= для объектов класса longe.
	* @param b множитель.
	* @return this,умноженное на b.
	* @see longe::operator* (const longe &b)
	*/
	longe& operator *= ( const longe& );
	/**
	* Перегрузка операции /= для объектов класса longe.
	* @param b делитель.
	* @return this,разделенное на b.
	* @see longe::operator/ (const longe &b)
	*/
	longe& operator /= ( const longe& );
	//void printall();
	//void print ();
};



