﻿#ifndef TEST_H
#define TEST_H

#include <cppunit/extensions/HelperMacros.h>
#include "longe.h"

using namespace CppUnit;
/**
*Класс как набор тестов для объетков класса longe
*/  
class LongTest : public TestFixture {
	CPPUNIT_TEST_SUITE(LongTest);

	CPPUNIT_TEST(Addition);
	CPPUNIT_TEST(Subtraction);
	CPPUNIT_TEST(Multiplication);
	CPPUNIT_TEST(Division);
	CPPUNIT_TEST(More);
	CPPUNIT_TEST(Less);
	CPPUNIT_TEST(MoreEqual);
	CPPUNIT_TEST(LessEqual);
	CPPUNIT_TEST(Equal);
	CPPUNIT_TEST(NotEqual);

	CPPUNIT_TEST_SUITE_END();
public:
    /**
    *Выделение памяти для данных объекта класса LongTest
    * 
    */    
	void setUp();
	/**
    *Очистка памяти для данных объекта класса LongTest
    */
	void tearDown();
    /**
    *Тест - проверка сложения
    */
	void Addition();
	/**
    *Тест - проверка вычитания
    */
	void Subtraction();
	/**
    *Тест - проверка умножения
    */
	void Multiplication();
	/**
    *Тест - проверка деления
    */
	void Division();
	/**
    *Тест - проверка "больше"
    */
	void More();
	/**
    *Тест - проверка "меньше"
    */
	void Less();
	/**
    *Тест - проверка "больше-равно"
    */
	void MoreEqual();
	/**
    *Тест - проверка "меньше-равно"
    */
	void LessEqual();
	/**
    *Тест - проверка на равенсто
    */
	void Equal(); 
	/**
    *Тест - проверка на неравенство
    */
	void NotEqual(); 
private:
	longe *a,*b,*c;

};


#endif //TEST_H
